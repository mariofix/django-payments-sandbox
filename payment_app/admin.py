from django.contrib import admin
from payment_app.models import Payment


class PaymentFilter(admin.ModelAdmin):
    list_display = ["variant", "status", "total", "billing_first_name"]


admin.site.register(Payment, PaymentFilter)
