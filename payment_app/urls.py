from django.urls import path
from payment_app import views


app_name = "payment_app"
urlpatterns = [
    path("payment-details/<int:payment_id>", views.payment_details),
    path("payment-success", views.payment_success),
    path(
        "payment-failure",
        views.payment_failure,
    ),
    path("payment-create/", views.create_test_payment),
]
