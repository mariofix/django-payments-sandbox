from django.db import models
from payments import PurchasedItem
from payments.models import BasePayment


class Payment(BasePayment):
    def get_failure_url(self) -> str:
        print("payment_app.models.Payment.get_failure_url()")
        return "http://nagini.local:8000/app/payment-failure"

    def get_success_url(self) -> str:
        print("payment_app.models.Payment.get_success_url()")
        return "http://nagini.local:8000/app/payment-success"

    def get_purchased_items(self) -> PurchasedItem:
        print("payment_app.models.Payment.get_purchased_items()")
        # Return items that will be included in this payment.
        yield PurchasedItem(
            name=self.description,
            sku="BSKV",
            quantity=1,
            price=self.total,
            currency=self.currency,
        )
